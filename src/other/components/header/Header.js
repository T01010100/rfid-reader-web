import './Header.css'
import React from "react";
import Wrapper from "../wrapper/Wrapper";

function Header(props) {
    return (
        <div className="header">
            <Wrapper>
                <h1>Dungeons and Dragons</h1>
            </Wrapper>
        </div>
    );
}

export default Header;
