import './SkillBox.css';
import React, {Component} from 'react';

class SkillBox extends Component {
    render() {
        return (
            <div className="skill-box box">
                <img src={this.props.boxType} alt="box design"/>
                <div className="skill-list">
                    {this.props.children}
                </div>
                <span className="box-label">{this.props.boxLabel}</span>
            </div>
        );
    }
}

export default SkillBox;
