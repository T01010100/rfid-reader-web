import './Header.css'
import React, {Component} from "react";
import header from "../../img/header.png";

class Header extends Component {
    render() {
        const data = this.props.data;

        return (
            <div className="header">
                <img src={header} alt="header"/>
                <span className="name">{data.nam}</span>
                <span className="name-tag">CHARACTER NAME</span>
                <span className="class">{data.cls} lvl. {data.lvl}</span>
                <span className="class-tag">CLASS & LEVEL</span>
                <span className="background">{data.bck}</span>
                <span className="background-tag">BACKGROUND</span>
                <span className="player-tag">PLAYER NAME</span>
                <span className="race">{data.rce}</span>
                <span className="race-tag">RACE</span>
                <span className="alignment">{this.getAlignment(data.agn)}</span>
                <span className="alignment-tag">ALIGNMENT</span>
                <span className="xp">{data.exp}</span>
                <span className="xp-tag">EXPERIENCE POINTS</span>
            </div>
        );
    }

    getAlignment(alignment) {
        switch (alignment) {
            case "lg":
                return "Lawful Good";
            case "ln":
                return "Lawful Neutral";
            case "le":
                return "Lawful Evil";
            case "ng":
                return "Neutral Good";
            case "nn":
                return "True Neutral";
            case "ne":
                return "Neutral Evil";
            case "cg":
                return "Chaotic Good";
            case "cn":
                return "Chaotic Neutral";
            case "ce":
                return "Chaotic Evil";
            default:
                return "";
        }
    }
}

export default Header;
