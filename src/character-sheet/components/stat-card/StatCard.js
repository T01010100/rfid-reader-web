import statCard from '../../img/stat_card.png';
import './StatCard.css';
import {Component} from 'react';

class StatCard extends Component {
    render() {
        return (
            <div className="stat-card">
                <img src={statCard} alt="card design" />
                <span className="label">{this.props.data.label.toUpperCase()}</span>
                <span className="modifier">{this.withSign(StatCard.calcModifier(this.props.data.value))}</span>
                <span className="value">{this.props.data.value}</span>
            </div>
        )
    }

    static calcModifier(value) {
        return Math.floor((value - 10) / 2)
    }

    withSign(input) {
        if (input > 0) {
            return '+'+input;
        }
        return input;
    }
}

export default StatCard
