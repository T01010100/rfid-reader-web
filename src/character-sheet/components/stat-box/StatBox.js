import './StatBox.css';
import React, {Component} from 'react';

class StatBox extends Component {
    render() {
        return (
            <div className="stat-box box">
                <img src={this.props.boxType} alt="box design"/>
                <span className={"box-value " + this.props.valueAlignment}>{this.props.boxValue}</span>
                <span className="box-label">{this.props.boxLabel.toUpperCase()}</span>
            </div>
        );
    }
}

export default StatBox;
