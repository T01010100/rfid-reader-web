import statBar from '../../img/stat_column.png';
import './StatBar.css';
import {Component} from 'react';
import StatCard from "../stat-card/StatCard";

class StatBar extends Component {
    render() {
        const baseData = this.props.data;

        return (
            <div className="stat-bar">
                <img src={statBar} alt="background design"/>
                <div className="stat-bar-container">
                    <StatCard data={{label: 'strength', value: baseData.str}} />
                    <StatCard data={{label: 'dexterity', value: baseData.dex}} />
                    <StatCard data={{label: 'constitution', value: baseData.con}} />
                    <StatCard data={{label: 'intelligence', value: baseData.int}} />
                    <StatCard data={{label: 'wisdom', value: baseData.wis}} />
                    <StatCard data={{label: 'charisma', value: baseData.cha}} />
                </div>
            </div>
        )
    }
}

export default StatBar
