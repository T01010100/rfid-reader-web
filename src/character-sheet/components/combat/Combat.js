import './Combat.css';
import React, {Component} from 'react';
import combatBox from "../../img/combat_box.png";
import armorClass from "../../img/armor_class.png";
import initiative from "../../img/initiative.png";
import speed from "../../img/speed.png";
import hitPoints from "../../img/hit_points.png";
import temporaryHitPoints from "../../img/temporary_hit_points.png";
import hitDice from "../../img/hit_dice.png";
import deathSaves from "../../img/death_saves.png";
import StatCard from "../stat-card/StatCard";

class Combat extends Component {
    render() {
        const data = this.props.data;

        return (
            <div className="combat-box">
                <img src={combatBox} alt="rectangle"/>
                <div className="combat">
                    <div className="armor-class">
                        <div style={{position: "relative"}}>
                            <img src={armorClass} alt="shield"/>
                            <span className="box-label" style={{bottom: "40px", lineHeight: "26px"}}>ARMOR<br/>CLASS</span>
                            <span className="box-value">{10 + StatCard.calcModifier(data.bse.dex)}</span>
                        </div>
                    </div>
                    <div className="initiative">
                        <img src={initiative} alt="box"/>
                        <span className="box-label" style={{bottom: "30px"}}>INITIATIVE</span>
                        <span className="box-value">?</span>
                    </div>
                    <div className="speed">
                        <img src={speed} alt="box"/>
                        <span className="box-label" style={{bottom: "30px"}}>SPEED</span>
                        <span className="box-value">{data.gen.spd}</span>
                    </div>
                    <div className="hit-points">
                        <img src={hitPoints} alt="box"/>
                        <span className="box-label">CURRENT HIT POINTS</span>
                        <span className="box-tag" style={{left: "350px"}}>{data.gen.mhp}</span>
                    </div>
                    <div className="temporary-hits">
                        <img src={temporaryHitPoints} alt="box"/>
                        <span className="box-label">TEMPORARY HIT POINTS</span>
                    </div>
                    <div className="hit-dice">
                        <div style={{position: "relative"}}>
                            <img src={hitDice} alt="box"/>
                            <span className="box-label">HIT DICE</span>
                            <span className="box-tag">{data.gen.lvl}</span>
                        </div>
                    </div>
                    <div className="death-saves">
                        <div style={{position: "relative"}}>
                            <img src={deathSaves} alt="box"/>
                            <span className="box-label">DEATH SAVES</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Combat;
