import './CharacterSheet.css'
import React, {Component} from "react";
import Header from "../header/Header";
import StatBar from "../stat-bar/StatBar";
import StatBox from "../stat-box/StatBox";
import inspiration from "../../img/inspiration.png";
import proficiencyBonus from "../../img/proficiency.png";
import SkillBox from "../skill-box/SkillBox";
import savingThrowBox from "../../img/saving_throw_box.png";
import Skill from "../skill/Skill";
import skillBox from "../../img/skill_box.png";
import Combat from "../combat/Combat";
import skillBoxes from "./skill_boxes.json"

class CharacterSheet extends Component {
    render() {
        let data = this.props.data;

        const proficiency = this.calcProficiency(data.dta.gen.lvl);

        const boxes = [];

        for (const box of skillBoxes) {
            const skills = [];

            for (const skill of box.skills) {
                const tag = skill.hasOwnProperty("alias") ? skill.alias : skill.type;
                skills.push(
                    <Skill label={skill.name} value={data.dta.bse[skill.type]} proficient={this.isProficient(data.dta.skl, tag)} proficiency={proficiency} />
                )
            }

            boxes.push(
                <SkillBox boxType={box.name === "Skills" ? skillBox : savingThrowBox} boxLabel={box.name.toUpperCase()}>
                    {skills}
                </SkillBox>
            );
        }

        return (
            <div className="sheet">
                <Header data={data.dta.gen} />
                <div className="content">
                    <StatBar data={data.dta.bse} />
                    <div className="skill-row">
                        <StatBox boxType={inspiration} boxValue={"?"} boxLabel="inspiration" valueAlignment="inspiration" />
                        <StatBox boxType={proficiencyBonus} boxValue={proficiency} boxLabel="proficiency bonus" />
                        {boxes}
                    </div>
                    <Combat data={data.dta} />
                </div>
            </div>
        );
    }

    calcProficiency(level) {
        return Math.floor((level - 1) / 4) + 2;
    }

    isProficient(skills, tag) {
        return skills.filter(skill => skill === tag).length > 0
    }
}

export default CharacterSheet;
