import skillCircle from '../../img/skill_circle.png';
import skillTick from '../../img/skill_tick.png';
import skillLine from '../../img/skill_line.png';
import './Skill.css';
import React, {Component} from 'react';
import StatCard from "../stat-card/StatCard";

class Skill extends Component {
    render() {
        return (
            <div className="skill">
                <div className="circle">
                    <img src={skillCircle} alt="circle"/>
                    {this.props.proficient ? <img src={skillTick} alt="tick" className="tick"/> : ''}
                </div>
                <div className="value-box">
                    <span className="value">{this.calcModifier(StatCard.calcModifier(this.props.value), this.props.proficient, this.props.proficiency)}</span>
                    <img className="line" src={skillLine} alt="line"/>
                </div>
                <span className="label">{this.props.label} {this.props.type ? <span className="type">({this.props.type})</span> : ''}</span>
            </div>
        );
    }

    calcModifier(standardModifier, proficient, proficiency) {
        if (proficient) {
            standardModifier += proficiency
        }
        return standardModifier;
    }
}

export default Skill;
