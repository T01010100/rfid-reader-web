import "./CreateFigureView.css"
import React, {Component} from "react";
import AlignmentPicker from "../alignment-picker/AlignmentPicker";
import LevelBar from "../level-bar/LevelBar";
import FigureView from "../figure-view/FigureView";
import FigureOverview from "../figure-overview/FigureOverview";
import Wrapper from "../../../other/components/wrapper/Wrapper";

function StageSetName(name, updateName) {
    return (
        <div>
            <h1>First, whats your character's name?</h1>
            <input value={ name } onChange={ event => updateName(event.target.value) }/>
        </div>
    );
}

function StageSetRaceAndClass(race, cls, updateClass) {
    return (
        <div>
            <h1>And what's your character's race and class?</h1>
            <select onChange={ event => updateClass(event.target.value, null) }>
                <option value="Human" selected={race === "Human"}>Human</option>
                <option value="Elf" selected={race === "Elf"}>Elf</option>
            </select>
            <select onChange={ event => updateClass(null, event.target.value) }>
                <option value="Fighter" selected={cls === "Fighter"}>Fighter</option>
                <option value="Warlock" selected={cls === "Warlock"}>Warlock</option>
            </select>
        </div>
    );
}

function StageSetStats(stats, updateStats) {
    return (
        <div>
            <h1>Now choose your character's stats</h1>
            <span>Strength</span>
            <input type="number" value={stats.str} onChange={ event => updateStats("str", event.target.value) } min={0} max={15}/>
            <span>Dexterity</span>
            <input type="number" value={stats.dex} onChange={ event => updateStats("dex", event.target.value) } min={0} max={15}/>
            <span>Constitution</span>
            <input type="number" value={stats.con} onChange={ event => updateStats("con", event.target.value) } min={0} max={15}/>
            <span>Intelligence</span>
            <input type="number" value={stats.int} onChange={ event => updateStats("int", event.target.value) } min={0} max={15}/>
            <span>Wisdom</span>
            <input type="number" value={stats.wis} onChange={ event => updateStats("wis", event.target.value) } min={0} max={15}/>
            <span>Charisma</span>
            <input type="number" value={stats.cha} onChange={ event => updateStats("cha", event.target.value) } min={0} max={15}/>
        </div>
    );
}

function StageSetOther(value, updateAlignment) {
    return (
        <div>
            <h1>Now you can finish up your character</h1>
            <AlignmentPicker value={value} onChange={ val => updateAlignment(val) } />
        </div>
    );
}

function StageOverview(data) {
    return (
        <FigureOverview data={data} />
    );
}

class CreateFigureView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: 5,
            data: {
                name: "Test Name",
                race: "Human",
                class: "Fighter",
                alignment: "nn",
                base: {
                    str: 5,
                    dex: 5,
                    con: 5,
                    int: 5,
                    wis: 5,
                    cha: 5
                }
            }
        };
    }

    updateFigure() {
        const data = this.state.data;

        const figureData = {
            "gen": {
                "nam": data.name,
                "cls": data.class,
                "rce": data.race,
                "bck": "Soldier",
                "exp": 0,
                "lvl": 1,
                "agn": data.alignment,
                "spd": 30,
                "mhp": 0
            },
            "bse": data.base,
            "skl": []
        }

        this.props.updateFigure(figureData);
    }

    render() {
        const data = this.state.data;
        const stage = this.state.stage;

        let stageView
        switch (stage) {
            case 1:
                stageView = StageSetName(data.name, name => this.updateName(name));
                break;
            case 2:
                stageView = StageSetRaceAndClass(data.race, data.class, (race, cls) => this.updateClass(race, cls));
                break;
            case 3:
                stageView = StageSetStats(data.base, (statName, stat) => this.updateStats(statName, stat));
                break;
            case 4:
                stageView = StageSetOther(data.alignment, (alignment) => this.updateAlignment(alignment));
                break;
            default:
                stageView = StageOverview(data);
                break;
        }

        const back = stage > 1 ? <button className="button" onClick={ () => this.previousStage() }>Back</button> : null;
        const next = stage < 5 ? <button className="button button-primary" onClick={ () => this.nextStage() }>Next</button> : <button className="button button-primary" onClick={ () => this.updateFigure() }>Finish</button>;

        return (
            <div className="create-figure">
                <Wrapper>
                    {stageView}
                    <div className="button-menu">
                        {back}
                        {next}
                    </div>
                </Wrapper>
            </div>
        );
    }

    previousStage() {
        this.setState({ stage: this.state.stage - 1 });
    }

    nextStage() {
        this.setState({ stage: this.state.stage + 1 });
    }

    updateName(name) {
        const data = this.state.data;
        data.name = name;
        this.setState({ data });
    }

    updateClass(race, cls) {
        const data = this.state.data;
        if (race) data.race = race;
        if (cls) data.class = cls;
        this.setState({ data });
    }

    updateStats(statName, stat) {
        const data = this.state.data;
        data.base[statName] = stat;
        this.setState({ data });
    }

    updateAlignment(alignment) {
        const data = this.state.data;
        data.alignment = alignment;
        this.setState(data);
    }
}

export default CreateFigureView;
