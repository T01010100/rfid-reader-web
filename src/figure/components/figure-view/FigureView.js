import './FigureView.css'
import React, {Component} from "react";
import CreateFigureView from "../create-figure-view/CreateFigureView";

class FigureView extends Component {
    render() {

        return (
            <div className="figure">
                <CreateFigureView />
            </div>
        );
    }
}

export default FigureView;
