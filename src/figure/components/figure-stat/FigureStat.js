import './FigureStat.css'
import React, {Component} from "react";
import StatCard from "../../../character-sheet/components/stat-card/StatCard";

class FigureStat extends Component {
    render() {
        const name = this.props.name;
        const amount = this.props.amount;

        return (
            <div className="figure-stat" style={ this.props.left ? { marginLeft: "0" } : this.props.right ? { marginRight: "0" } : {}}>
                <span className="figure-stat-name">{name}</span>
                <span className="figure-stat-modifier">{StatCard.calcModifier(amount)}</span>
                <span className="figure-stat-amount">{amount}</span>
            </div>
        );
    }
}

export default FigureStat;
