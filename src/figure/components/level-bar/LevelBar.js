import './LevelBar.css'
import React, {Component} from "react";

class LevelBar extends Component {
    static expTable = [
        0,
        300,
        900,
        2700,
        6500,
        14000,
        23000,
        34000,
        48000,
        64000,
        85000,
        100000,
        120000,
        140000,
        165000,
        195000,
        225000,
        265000,
        305000,
        355000
    ];

    render() {
        const level = this.props.level;
        const exp = this.props.exp;
        const updateExp = this.props.updateExp;

        const reqExp = level !== 20 ? LevelBar.getRequiredExp(level) : "-";
        const offset = LevelBar.getRequiredExp(level - 1);

        const updateReqExp = LevelBar.getRequiredExp(level + 1);
        const updateOffset = LevelBar.getRequiredExp(level);

        let progressBar;
        if (level !== 20) {
            let bars = [];
            if (updateExp && updateExp > 0) {
                bars.push(
                    <div className="progress update-progress"
                         style={{width: (100 / (reqExp - offset) * (exp + updateExp - offset)) + "%"}}/>
                );
            }

            bars.push(
                <div className="progress" style={{width: (100 / (reqExp - offset) * (exp - offset)) + "%"}} />
            );

            if (exp + updateExp > reqExp) {
                bars.push(
                    <div className="progress update-progress" style={{width: (100 / (updateReqExp - updateOffset) * (exp + updateExp - updateOffset)) + "%"}} />
                );
            }

            progressBar = (
                <div className="progress-bar">
                    {bars}
                </div>
            );
        } else {
            progressBar = (
                <div className="progress-bar">
                    <div className="progress max-progress" />
                </div>
            );
        }

        return (
            <div className="level-bar">
                {progressBar}
                <div className="level-status">
                    <span className="level-value">lvl. {level}</span>
                    <span className="exp-value">{exp}/{reqExp}</span>
                </div>
            </div>
        );
    }

    static getRequiredExp(level) {
        return LevelBar.expTable[level];
    }
}

export default LevelBar;
