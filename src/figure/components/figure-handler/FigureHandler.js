import './FigureHandler.css'
import React, {Component} from "react";
import CreateFigureView from "../create-figure-view/CreateFigureView";
import md5 from "md5"

class FigureHandler extends Component {
    updateFigure(data) {
        const figure = {
            dta: data,
            vld: md5(data)
        }

        this.props.sendWebsocket("update-figure", figure);
    }

    render() {
        const data = this.props.data;

        // todo maybe remove this component? lmao

        return (
            <div className="figure">
                <CreateFigureView data={data} updateFigure={ (data) => this.updateFigure(data) } />
            </div>
        );
    }
}

export default FigureHandler;
