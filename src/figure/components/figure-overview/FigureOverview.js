import './FigureOverview.css'
import React, {Component} from "react";
import LevelBar from "../level-bar/LevelBar";
import AlignmentPicker from "../alignment-picker/AlignmentPicker";
import FigureStat from "../figure-stat/FigureStat";

class FigureOverview extends Component {
    render() {
        const data = this.props.data;

        return (
            <div className="figure-overview">
                <div className="figure-header">
                    <span className="figure-name">{data.name}</span>
                    <div className="figure-details">
                        <span className="figure-class">{data.race} - {data.class}</span>
                        <LevelBar level={20} exp={150} updateExp={500} />
                    </div>
                </div>
                <div className="figure-stat-list">
                    <FigureStat name="Str" amount={data.base.str} left={true} />
                    <FigureStat name="Dex" amount={data.base.dex} />
                    <FigureStat name="Con" amount={data.base.con} />
                    <FigureStat name="Int" amount={data.base.int} />
                    <FigureStat name="Wis" amount={data.base.wis} />
                    <FigureStat name="Cha" amount={data.base.cha} right={true} />
                </div>
                <h4>{AlignmentPicker.translateAlignment(data.alignment)}</h4>
            </div>
        );
    }
}

export default FigureOverview;
