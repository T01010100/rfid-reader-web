import './AlignmentPicker.css'
import React, {Component} from "react";

class AlignmentPicker extends Component {
    constructor(props) {
        super(props);

        this.state = { dialogOpen: false }
    }

    render() {
        let value = this.props.value;

        if (!value) value = "nn";

        const selectorView = this.state.dialogOpen ? (
            <div className="selector">
                <span className="lg" onClick={() => this.applyValue("lg")}>Lawful Good</span>
                <span className="ng" onClick={() => this.applyValue("ng")}>Neutral Good</span>
                <span className="cg" onClick={() => this.applyValue("cg")}>Chaotic Good</span>
                <span className="ln" onClick={() => this.applyValue("ln")}>Lawful Neutral</span>
                <span className="nn" onClick={() => this.applyValue("nn")}>True Neutral</span>
                <span className="cn" onClick={() => this.applyValue("cn")}>Chaotic Neutral</span>
                <span className="le" onClick={() => this.applyValue("le")}>Lawful Evil</span>
                <span className="ne" onClick={() => this.applyValue("ne")}>Neutral Evil</span>
                <span className="ce" onClick={() => this.applyValue("ce")}>Chaotic Evil</span>
            </div>
        ) : null;

        return (
            <div className="alignment-picker">
                <p onClick={() => this.toggleDialog()}>{AlignmentPicker.translateAlignment(value)}</p>
                {selectorView}
            </div>
        );
    }

    toggleDialog() {
        this.setState({ dialogOpen: !this.state.dialogOpen });
    }

    applyValue(value) {
        const onChange = this.props.onChange;
        if (onChange) onChange(value);
        this.setState({ dialogOpen: false });
    }

    static translateAlignment(key) {
        switch (key) {
            case "lg":
                return "Lawful Good";
            case "ln":
                return "Lawful Neutral";
            case "le":
                return "Lawful Evil";
            case "ng":
                return "Neutral Good";
            case "nn":
                return "True Neutral";
            case "ne":
                return "Neutral Evil";
            case "cg":
                return "Chaotic Good";
            case "cn":
                return "Chaotic Neutral";
            case "ce":
                return "Chaotic Evil";
            default:
                return "No Alignment Found"
        }
    }
}

export default AlignmentPicker;
