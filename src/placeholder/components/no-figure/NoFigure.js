import './NoFigure.css'
import React, {Component} from "react";

class NoFigure extends Component {
    render() {

        return (
            <div className="no-figure">
                <h1>No figure connected</h1>
                <p>Place a figure on the device to see his stats.</p>
            </div>
        );
    }
}

export default NoFigure;
