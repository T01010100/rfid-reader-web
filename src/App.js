import './App.css';
import React, {Component} from 'react';
import NoFigure from "./placeholder/components/no-figure/NoFigure";
import FigureHandler from "./figure/components/figure-handler/FigureHandler";
import Header from "./other/components/header/Header";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      websocket: null,
      data: null,
      hasFigure: 0
    };
  }

  openWebsocketConnection() {
    const websocket = new WebSocket("ws://localhost:8765");

    websocket.onopen = () => {
      console.log("established websocket connection");

      this.sendWebsocket("connection-request", "web-app");
    }

    websocket.onmessage = (message) => {
      console.log("> " + message.data);
      this.setState({ data: JSON.parse(message.data) });
    }

    websocket.onclose = () => {
      console.log("websocket connection closed");
    }

    websocket.onerror = () => {
      console.log("websocket error");
    }

    this.setState({ websocket });
  }

  sendWebsocket(action, message) {
    console.log("< " + action + " " + message);
    this.state.websocket.send(JSON.stringify({
      action,
      message
    }))
  }

  render() {
    let data = this.state.data;
    if (this.state.hasFigure && data === "") {
      data = {
        "dta": {
          "gen": {
            "nam": "Peter der Grosse",
            "cls": "Fighter",
            "rce": "Human",
            "bck": "Soldier",
            "exp": 0,
            "lvl": 1,
            "agn": "nn",
            "spd": 30,
            "mhp": 0
          },
          "bse": {
            "str": 0,
            "dex": 0,
            "con": 0,
            "int": 0,
            "wis": 0,
            "cha": 0
          },
          "skl": []
        },
        "vld": "098f6bcd4621d373cade4e832627b4f6"
      };
    }
    data = {};

    const charSheet = data ? (<FigureHandler data={data} sendWebsocket={ (action, data) => this.sendWebsocket(action, data) } />) : (<NoFigure />)

    return (
      <div className="App">
        <Header />
        <div className="app-content">
          {charSheet}
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.openWebsocketConnection();
  }
}

export default App;
